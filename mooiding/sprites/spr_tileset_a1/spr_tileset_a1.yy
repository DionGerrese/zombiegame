{
    "id": "0f939b4e-1ca5-4e5b-b7d0-e69228467191",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_a1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "024180c4-dc7b-475f-86e8-b694caea33b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f939b4e-1ca5-4e5b-b7d0-e69228467191",
            "compositeImage": {
                "id": "a79693f1-037a-4567-b999-17d9d5f504d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "024180c4-dc7b-475f-86e8-b694caea33b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2be3281-7624-4cc5-ba35-47b1c1bc43dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "024180c4-dc7b-475f-86e8-b694caea33b1",
                    "LayerId": "29fc08ef-d729-4fd2-92d6-3fa92598fe44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "29fc08ef-d729-4fd2-92d6-3fa92598fe44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f939b4e-1ca5-4e5b-b7d0-e69228467191",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}