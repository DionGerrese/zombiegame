{
    "id": "0c856058-0c71-4d91-aba5-be444d2aa321",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_circle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a85eb95f-9bc8-4380-97c6-209c06aba965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c856058-0c71-4d91-aba5-be444d2aa321",
            "compositeImage": {
                "id": "bd70e713-562f-4557-92f2-9107f3edf742",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a85eb95f-9bc8-4380-97c6-209c06aba965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf31ca32-d090-4265-941a-44c6658eff1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a85eb95f-9bc8-4380-97c6-209c06aba965",
                    "LayerId": "dbb9cd66-aed0-4553-92f5-23c20ad409d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dbb9cd66-aed0-4553-92f5-23c20ad409d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c856058-0c71-4d91-aba5-be444d2aa321",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}