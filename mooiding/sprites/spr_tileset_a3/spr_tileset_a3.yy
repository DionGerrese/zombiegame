{
    "id": "0273d8b9-d2ec-4052-9ae0-3d1c22cfe088",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_a3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 271,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3a1634af-0c6c-4268-8cc4-3850f6280ec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0273d8b9-d2ec-4052-9ae0-3d1c22cfe088",
            "compositeImage": {
                "id": "53474dd0-9c95-415f-8775-cb06cc562ff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a1634af-0c6c-4268-8cc4-3850f6280ec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47ca6d5d-2081-4137-84ea-407473611164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a1634af-0c6c-4268-8cc4-3850f6280ec5",
                    "LayerId": "95befd95-4105-49e3-9495-556675cb425a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 272,
    "layers": [
        {
            "id": "95befd95-4105-49e3-9495-556675cb425a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0273d8b9-d2ec-4052-9ae0-3d1c22cfe088",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}