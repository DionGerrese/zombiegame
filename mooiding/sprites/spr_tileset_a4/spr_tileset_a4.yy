{
    "id": "6b3358e1-0f1b-4b06-b504-2cf20cf9cb04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_a4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 495,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "21a52f9a-78cf-48c3-8d4c-84191aac63c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b3358e1-0f1b-4b06-b504-2cf20cf9cb04",
            "compositeImage": {
                "id": "8688c9a4-e9a4-4b2f-9e27-223e0cf3f24e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21a52f9a-78cf-48c3-8d4c-84191aac63c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2102709c-83b5-4e86-a035-08ed360915dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21a52f9a-78cf-48c3-8d4c-84191aac63c2",
                    "LayerId": "d6f27fb9-a921-44c4-ac30-c05680cd5e8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 496,
    "layers": [
        {
            "id": "d6f27fb9-a921-44c4-ac30-c05680cd5e8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b3358e1-0f1b-4b06-b504-2cf20cf9cb04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}