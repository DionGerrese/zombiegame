{
    "id": "7bee125e-519d-4d05-b3b9-e713dcaf5b80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "53ebf4c2-b195-4e0f-a24b-f7e8e97cea3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bee125e-519d-4d05-b3b9-e713dcaf5b80",
            "compositeImage": {
                "id": "60961167-f90c-41ef-a4b4-edd9b328cdda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53ebf4c2-b195-4e0f-a24b-f7e8e97cea3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd259dcd-9a9d-493b-8978-cf99f7dd30e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53ebf4c2-b195-4e0f-a24b-f7e8e97cea3a",
                    "LayerId": "fe53e530-0bc3-4bd6-b70c-49e52bdac256"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "fe53e530-0bc3-4bd6-b70c-49e52bdac256",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bee125e-519d-4d05-b3b9-e713dcaf5b80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}