{
    "id": "ef3da68d-dc12-4b6c-b9c6-1f18d620f27b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_a2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b70c17bd-5dcb-479a-b37a-194b637c887a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef3da68d-dc12-4b6c-b9c6-1f18d620f27b",
            "compositeImage": {
                "id": "d39b08a5-a248-485b-9711-123f76df6e21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b70c17bd-5dcb-479a-b37a-194b637c887a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc9bd05b-935f-432f-949e-7f1e319534c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b70c17bd-5dcb-479a-b37a-194b637c887a",
                    "LayerId": "591c1c93-31fa-4be6-a932-862c8e7093e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "591c1c93-31fa-4be6-a932-862c8e7093e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef3da68d-dc12-4b6c-b9c6-1f18d620f27b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}