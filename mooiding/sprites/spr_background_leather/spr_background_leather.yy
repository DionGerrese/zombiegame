{
    "id": "5f4b1692-47e5-4de2-baa7-5725adbc0e9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background_leather",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "40912b6d-4e50-4b4a-a52d-31c61ad59051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f4b1692-47e5-4de2-baa7-5725adbc0e9d",
            "compositeImage": {
                "id": "caaf8c20-50f8-44a0-984f-7455a60fa7af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40912b6d-4e50-4b4a-a52d-31c61ad59051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94af9341-38d5-47f8-b985-831d03b68def",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40912b6d-4e50-4b4a-a52d-31c61ad59051",
                    "LayerId": "2853518c-6529-433b-9c71-8b5b47ed6f56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "2853518c-6529-433b-9c71-8b5b47ed6f56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f4b1692-47e5-4de2-baa7-5725adbc0e9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}