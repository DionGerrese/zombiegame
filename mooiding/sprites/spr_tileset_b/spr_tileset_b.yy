{
    "id": "2cc26533-59d8-424b-9386-963d18b2dffc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "12dd0cb9-ab12-4050-a5bb-8bc9999a61e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cc26533-59d8-424b-9386-963d18b2dffc",
            "compositeImage": {
                "id": "8c34d3cb-3795-4d23-be94-b181be72efb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12dd0cb9-ab12-4050-a5bb-8bc9999a61e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c36e96dd-2cc5-47f6-acbb-7e5495fdfa44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12dd0cb9-ab12-4050-a5bb-8bc9999a61e4",
                    "LayerId": "715475af-3289-480e-9852-55c3e7327c27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "715475af-3289-480e-9852-55c3e7327c27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cc26533-59d8-424b-9386-963d18b2dffc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}