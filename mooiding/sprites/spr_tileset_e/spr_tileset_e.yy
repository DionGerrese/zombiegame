{
    "id": "0431e057-4e37-4b1b-8064-55b097da46a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_e",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fcc16711-50a8-4e56-80dc-ddf70350d659",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0431e057-4e37-4b1b-8064-55b097da46a3",
            "compositeImage": {
                "id": "119c5392-8058-432b-8308-bd3e3413fb83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcc16711-50a8-4e56-80dc-ddf70350d659",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "401bb4c4-c668-4c70-a421-57fadabf2f40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcc16711-50a8-4e56-80dc-ddf70350d659",
                    "LayerId": "bb6dd91f-f671-4b7c-b808-2efda7409dcd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "bb6dd91f-f671-4b7c-b808-2efda7409dcd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0431e057-4e37-4b1b-8064-55b097da46a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}