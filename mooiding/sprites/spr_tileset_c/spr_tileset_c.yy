{
    "id": "a4846f0b-7a6e-4e88-b0e0-f9502e0d3d15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 16,
    "bbox_right": 527,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "34ff19cd-7a82-41a4-9312-fce1052c0a54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4846f0b-7a6e-4e88-b0e0-f9502e0d3d15",
            "compositeImage": {
                "id": "3b90718f-3bf8-4ca2-8ea8-997b189fad2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34ff19cd-7a82-41a4-9312-fce1052c0a54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0805d7a-fc20-413a-b253-946cb6e21318",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ff19cd-7a82-41a4-9312-fce1052c0a54",
                    "LayerId": "fde0787a-c114-41e4-a52d-3d4c6ca00011"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "fde0787a-c114-41e4-a52d-3d4c6ca00011",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4846f0b-7a6e-4e88-b0e0-f9502e0d3d15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 528,
    "xorig": 0,
    "yorig": 0
}