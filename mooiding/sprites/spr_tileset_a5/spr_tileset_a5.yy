{
    "id": "7a9a1a4b-76ec-443c-94ae-18370cf8bfcc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_a5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 16,
    "bbox_right": 271,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "dbc10e0d-c923-4219-823c-f00e2307c451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a9a1a4b-76ec-443c-94ae-18370cf8bfcc",
            "compositeImage": {
                "id": "cbf06d10-c4e9-47a7-86b9-7126d048628d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbc10e0d-c923-4219-823c-f00e2307c451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0facf45-113c-417b-9fbc-d2bd9bd09e31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbc10e0d-c923-4219-823c-f00e2307c451",
                    "LayerId": "fe3190a9-72fa-4b42-bb36-45663ba1f6bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "fe3190a9-72fa-4b42-bb36-45663ba1f6bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a9a1a4b-76ec-443c-94ae-18370cf8bfcc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 272,
    "xorig": 0,
    "yorig": 0
}