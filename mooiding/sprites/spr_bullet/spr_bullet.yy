{
    "id": "fa580b58-0793-4a83-a0c7-b194cfea83e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 4,
    "bbox_right": 59,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e9b88894-efa2-45ec-b901-4ca444017eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa580b58-0793-4a83-a0c7-b194cfea83e5",
            "compositeImage": {
                "id": "315c9172-a2d7-45e3-b585-da2f743e53a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9b88894-efa2-45ec-b901-4ca444017eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "964d99a0-5672-40b9-9880-394542f370cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b88894-efa2-45ec-b901-4ca444017eba",
                    "LayerId": "b30c0410-6d71-45ea-9336-57c7b50d86be"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 64,
    "layers": [
        {
            "id": "b30c0410-6d71-45ea-9336-57c7b50d86be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa580b58-0793-4a83-a0c7-b194cfea83e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}