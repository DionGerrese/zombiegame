{
    "id": "d967e3d0-c391-450a-8b0b-60efd34cf846",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_d",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e4bfe288-0859-49cc-808e-932e8e2fb207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d967e3d0-c391-450a-8b0b-60efd34cf846",
            "compositeImage": {
                "id": "169bcc1d-eaa7-4268-bbd5-e7f27bfea0ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4bfe288-0859-49cc-808e-932e8e2fb207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce706b0b-2c1a-4557-92df-100ae32cea6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4bfe288-0859-49cc-808e-932e8e2fb207",
                    "LayerId": "104dd21e-afea-416f-8ce4-96155659f0ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "104dd21e-afea-416f-8ce4-96155659f0ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d967e3d0-c391-450a-8b0b-60efd34cf846",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}