x += (x_to - x) / 8;
y += (y_to - y) / 8;

if (follow != noone)
{
	x_to = follow.x;
	y_to = follow.y;
}

var vm = matrix_build_lookat(x, y, -10, x, y, 0, 0, 1, 0);
camera_set_view_mat(cam, vm);

if (mouse_wheel_down())
{
	w_to = w * 1.20;
	h_to = h * 1.20;
}	
if (mouse_wheel_up())
{
	w_to = w * 0.80;
	h_to = h * 0.80;
}
	
w += (w_to - w) / 8;
h += (h_to - h) / 8;

w = clamp(w, view_wport[0] * 0.5, view_wport[0] * 2.5);
h = clamp(h, view_hport[0] * 0.5, view_hport[0] * 2.5);

var pm = matrix_build_projection_ortho(w, h, 1, 9999);
camera_set_proj_mat(cam, pm);