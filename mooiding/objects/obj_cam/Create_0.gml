cam = camera_create();

w = view_wport[0];
h = view_hport[0];

var vm = matrix_build_lookat(x, y, -10, x, y, 0, 0, 1, 0);
var pm = matrix_build_projection_ortho(w, h, 1, 9999);

camera_set_view_mat(cam, vm);
camera_set_proj_mat(cam, pm);

view_camera[0] = cam;

follow = obj_player;

x_to = x;
y_to = y;

w_to = w;
h_to = h;