{
    "id": "9a5a3d70-89ed-4be0-8777-fe4377b45764",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_sound",
    "eventList": [
        {
            "id": "809e63e4-dca1-40e7-8b8d-e0ecf9a5ff07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9a5a3d70-89ed-4be0-8777-fe4377b45764"
        },
        {
            "id": "f6f54459-4a6c-483f-bcfb-5b1d911d59df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9a5a3d70-89ed-4be0-8777-fe4377b45764"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}