{
    "id": "ad15bf9d-cba2-42f5-ae5a-f29fc7f3dd60",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_zomble",
    "eventList": [
        {
            "id": "108799b1-6bff-4676-a097-edbdad1754b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad15bf9d-cba2-42f5-ae5a-f29fc7f3dd60"
        },
        {
            "id": "a2810ff1-8c45-4c27-a3d7-39be184568f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ad15bf9d-cba2-42f5-ae5a-f29fc7f3dd60"
        },
        {
            "id": "271a4b32-4039-4115-86f4-e15d5d21f079",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ad15bf9d-cba2-42f5-ae5a-f29fc7f3dd60",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ad15bf9d-cba2-42f5-ae5a-f29fc7f3dd60"
        },
        {
            "id": "1da0962f-9aad-4b3d-a94d-65bdd9d1a8a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5cc739f8-81ce-483f-b4a0-a803a6650817",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ad15bf9d-cba2-42f5-ae5a-f29fc7f3dd60"
        },
        {
            "id": "ae9e6510-6fbc-4aa5-aab0-83fc6d09d82b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ad15bf9d-cba2-42f5-ae5a-f29fc7f3dd60"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "0c856058-0c71-4d91-aba5-be444d2aa321",
    "visible": true
}