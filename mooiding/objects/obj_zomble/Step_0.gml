image_angle = direction;

if (/*!collision_line(x, y, obj_player.x, obj_player.y, obj_wall, false, true) && */
	angle_difference(image_angle, point_direction(x, y, obj_player.x, obj_player.y)) <= fov / 2 &&
	angle_difference(image_angle, point_direction(x, y, obj_player.x, obj_player.y)) >= -fov / 2 &&
	distance_to_point(obj_player.x, obj_player.y) <= rng)
{
	seen_player = true;
	motion_add(point_direction(x, y, obj_player.x, obj_player.y), 0.5);
	tx = obj_player.x;
	ty = obj_player.y;
}
else
{
	speed -= 0.5;
	seen_player = false;
}

speed = clamp(speed, min_spd, max_spd);

if (hp <= 0)
	instance_destroy(self);

if (distance_to_object(obj_walk_sound) < 500)
{
	tx = obj_walk_sound.x;
	ty = obj_walk_sound.y;
}
else if (distance_to_object(obj_bullet_sound) < 1000)
{
	tx = obj_bullet_sound.x;
	ty = obj_bullet_sound.y;
}
if (distance_to_point(tx, ty,) > 0 && seen_player == false)
	move_towards_point(tx, ty, max_spd);