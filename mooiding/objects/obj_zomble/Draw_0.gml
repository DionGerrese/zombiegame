draw_healthbar(x - sprite_width /2, y - sprite_height /2 - 24, x + sprite_width /2, y - sprite_height/2 - 4, hp, c_black, c_red, c_green, 0, false, false);

// fov begin
draw_line(x, y, x + lengthdir_x(rng, image_angle - fov / 2), y + lengthdir_y(rng, image_angle - fov / 2));
draw_line(x, y, x + lengthdir_x(rng, image_angle + fov / 2), y + lengthdir_y(rng, image_angle + fov / 2));

for(i = -fov / 2; i < fov / 2; i += 10)
{
	draw_line(x + lengthdir_x(rng, image_angle + i),
	y + lengthdir_y(rng, image_angle  + i),
	x + lengthdir_x(rng, image_angle  + i + 10),
	y + lengthdir_y(rng, image_angle  + i + 10))
}
// fov end

draw_text(x + sprite_width, y + sprite_height, angle_difference(direction, point_direction(x, y, obj_player.x, obj_player.y)))

draw_self();