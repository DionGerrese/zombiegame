image_angle = point_direction(x, y, mouse_x, mouse_y);

if (keyboard_check(ord("W")))
	motion_add(90, 0.5);
if (keyboard_check(ord("A")))
	motion_add(180, 0.5);
if (keyboard_check(ord("S")))
	motion_add(270, 0.5);
if (keyboard_check(ord("D")))
	motion_add(0, 0.5);
if (speed > 0 && !keyboard_check(ord("W")) && !keyboard_check(ord("A")) && !keyboard_check(ord("S")) && !keyboard_check(ord("D")))
	speed -= 0.5;
	
speed = clamp(speed, 0, 8);

if (mouse_check_button_pressed(mb_left))
{
	instance_create_layer(x, y, "Instances", obj_bullet);
	//instance_create_layer(x, y, "Instances", obj_bullet_sound);
}	
	
if (speed > 0)
{
	//instance_create_layer(x, y, "Instances", obj_walk_sound);
}